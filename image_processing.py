"""Person detection using Unit Makers detection service."""
import logging
import voluptuous as vol

from homeassistant.components.image_processing import (
    CONF_ENTITY_ID,
    CONF_NAME,
    CONF_SOURCE,
    PLATFORM_SCHEMA,
    ImageProcessingEntity,
)
from homeassistant.const import ATTR_ENTITY_ID, CONF_API_KEY
from homeassistant.core import split_entity_id
import homeassistant.helpers.config_validation as cv

from typing import Dict
import requests
import json

_LOGGER = logging.getLogger(__name__)

EVENT_OBJECT_DETECTED = "umcv.object_detected"
URL = "https://api.umcv.tech/v1/detect"

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend(
    {
        vol.Required(CONF_API_KEY): cv.string,
    }
)


def setup_platform(hass, config, add_entities, discovery_info=None):
    """Set up the platform."""
    # Validate credentials by processing image.
    api_key = config[CONF_API_KEY]
    api = UnitMakers(api_key)

    entities = []
    for camera in config[CONF_SOURCE]:
        um = UnitMakersEntity(
            api,
            camera[CONF_ENTITY_ID],
            camera.get(CONF_NAME),
        )
        entities.append(um)

    add_entities(entities)

class UnitMakers:
    def __init__(self, api_key: str):
        self._api_key = api_key

    def detect(self, image: bytes) -> Dict:
        """Run detection on an image (bytes)."""
        headers = {"Authorization": "Bearer " + self._api_key}
        response = requests.post(
            URL,
            headers=headers,
            files=dict(file=image),
        )

        if response.status_code == 200:
            return response.json()["predictions"]
        elif response.status_code == 401:
            raise UnitMakersException(f"Bad API key")
        elif response.status_code == 429:
            raise UnitMakersException(f"Request limit reached")

class UnitMakersException(Exception):
    pass

class UnitMakersEntity(ImageProcessingEntity):
    """Create a um entity."""

    def __init__(self, api, camera_entity, name):
        """Init."""
        self._api = api
        self._camera = camera_entity
        if name:
            self._name = name
        else:
            camera_name = split_entity_id(camera_entity)[1]
            self._name = f"umcv_{camera_name}"

    def process_image(self, image):
        """Process an image."""
        detections = self._api.detect(image)

        self.hass.bus.fire(
            EVENT_OBJECT_DETECTED,
            {"camera": self._camera, "detections": detections},
        )

    @property
    def camera_entity(self):
        """Return camera entity id from process pictures."""
        return self._camera

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def should_poll(self):
        """Return True if entity has to be polled for state."""
        return False
