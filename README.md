# Simple Motion Detection for Home Assistant!

This integration solves motion detection as simple as posible by using a remote processing tool to send images and offloading your local setup. Meaning you would only have to create an account at https://unitmakers.dk and generate an API key. 
No extra server setup, no storing of images on our server and a decent amount of request included for free!

If you have more activity or mulitple cameras we offer a paid plan.

##### Three steps are needed in Home Assistant to start getting detections.

First setup the Image Processing, add this to your configuration.yaml file:
```
image_processing:
  - platform: umcv
    api_key: api_key
    source:
      - entity_id: camera.[entity_id]
```

Next step is to trigger image_procsssing.scan when motion happens. This can be solved by the follwing snippet:
```
alias: Send to Unit Makers
trigger:
  - platform: state
    entity_id: binary_sensor.camera_1_motion
    to: 'on'
    from: 'off'
action:
  - service: image_processing.scan
    target:
      entity_id: image_processing.umcv_[camera-entity-id]
```

Finally here's an example of a motion trigger when a person is detected:
```
alias: Person detected
trigger:
  - platform: event
    event_type: umcv.object_detected
    event_data:
      camera: camera.[entity_id]
condition:
  - condition: template
    value_template: >-
      {{ trigger.event.data.detections | selectattr('label','eq','person')|list|count > 0 }}
action:
  - service: system_log.write
    data_template:
      message: 'DEMO = {{trigger.event.data}}'
      level: warning
```
Current action will write to the system log.     

Another example to trigger a notification to the mobile app:
```
alias: Send detection of people to iPad
trigger:
  - platform: event
    event_type: umcv.object_detected
    event_data:
      camera: camera.[entity_id]
condition:
  - condition: template
    value_template: >-
      {{ trigger.event.data.detections |
      selectattr('label','eq','person')|list|count > 0 }}
action:
  - service: notify.mobile_app_[entity_id]
    data:
      message: Person detected!
```      
